MASS 300
Observed Limit: r < 1.6045
Expected  2.5%: r < 0.5237
Expected 16.0%: r < 0.7145
Expected 50.0%: r < 1.0273
Expected 84.0%: r < 1.5310
Expected 97.5%: r < 2.2217
MASS 500
Observed Limit: r < 0.7430
Expected  2.5%: r < 0.2461
Expected 16.0%: r < 0.3367
Expected 50.0%: r < 0.4902
Expected 84.0%: r < 0.7345
Expected 97.5%: r < 1.0742
MASS 700
Observed Limit: r < 0.5009
Expected  2.5%: r < 0.1727
Expected 16.0%: r < 0.2372
Expected 50.0%: r < 0.3467
Expected 84.0%: r < 0.5222
Expected 97.5%: r < 0.7695
MASS 900
Observed Limit: r < 0.3719
Expected  2.5%: r < 0.1327
Expected 16.0%: r < 0.1831
Expected 50.0%: r < 0.2686
Expected 84.0%: r < 0.4066
Expected 97.5%: r < 0.6004
MASS 1100
Observed Limit: r < 0.3144
Expected  2.5%: r < 0.1153
Expected 16.0%: r < 0.1582
Expected 50.0%: r < 0.2334
Expected 84.0%: r < 0.3534
Expected 97.5%: r < 0.5246
MASS 1300
Observed Limit: r < 0.2839
Expected  2.5%: r < 0.1052
Expected 16.0%: r < 0.1447
Expected 50.0%: r < 0.2129
Expected 84.0%: r < 0.3241
Expected 97.5%: r < 0.4794
MASS 1500
Observed Limit: r < 0.2637
Expected  2.5%: r < 0.0980
Expected 16.0%: r < 0.1352
Expected 50.0%: r < 0.1982
Expected 84.0%: r < 0.3033
Expected 97.5%: r < 0.4496
